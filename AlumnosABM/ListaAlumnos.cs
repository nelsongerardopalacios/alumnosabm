﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumnosABM
{
    class ListaAlumnos
    {
        static public List<Alumno> Alumnos = new List<Alumno>();

        

        public bool Add(int dni, String nombre, String apellido, String curso, DateTime fechaNacimiento, Pais pais,Ciudad ciudad)
        {
            try
            {
                Alumno alumnoNuevo = new Alumno(dni, nombre, apellido, curso, fechaNacimiento,pais,ciudad);
                
                Alumnos.Add(alumnoNuevo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool modificarAlumno(int dni, String nombre, String apellido, String curso, DateTime fechaNacimiento, Pais pais, Ciudad ciudad)
        {

            try
            {
                foreach (Alumno alumnoObj in Alumnos)
                {
                    if (alumnoObj.getDni() == dni)
                    {
                        /*alumnoObj.setNombre(nombre);
                        alumnoObj.setApellido(apellido);
                        alumnoObj.setCurso(curso);*/

                        alumnoObj.Modificar(dni, nombre, apellido, curso, fechaNacimiento,pais,ciudad);
                    }

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Alumno BuscarAlumnoDni(int dni)
        {
            Alumno alumno = null;
            
            foreach (Alumno alumnoObj in Alumnos)
            {
                if (alumnoObj.getDni() == dni)
                {
                    //cuando encuentro el alumno debo para el foreach 
                    alumno = alumnoObj;
                    break;
                }
            }
            return alumno;
        }

        public bool dniExiste(int dni)
        {
            bool retorno = false;
            foreach (Alumno alumnoObj in Alumnos)
            {
                if (alumnoObj.getDni() == dni)
                {
                    retorno=true;
                }
            }
            return retorno;
                
            
        }

        public List<int> obtenerListaDni()
        {
            /*List<int> listaDni = new List<int>();
            
            foreach (Alumno alumno in Alumnos)
            {
                listaDni.Add(alumno.getDni());
            }
            return listaDni;*/

            List<int> listadoDni = new List<int>();
            for (int i = 0; i < Alumnos.Count; i++)
            {
                listadoDni.Add(Alumnos[i].getDni());
            }
            return listadoDni;


        }

        public List<string> obtenerListaDniString()
        {
            /*List<int> listaDni = new List<int>();
            
            foreach (Alumno alumno in Alumnos)
            {
                listaDni.Add(alumno.getDni());
            }
            return listaDni;*/

            List<string> listadoDniString = new List<string>();
            for (int i = 0; i < Alumnos.Count; i++)
            {
                int dniInt = Alumnos[i].getDni();
                listadoDniString.Add(dniInt.ToString());
            }
            return listadoDniString;


        }

        public bool EliminarAlumno(int dni)
        {
            try
            {
                foreach (Alumno alumnoObj in Alumnos)
                {
                    if (alumnoObj.getDni() == dni)
                    {
                        Alumnos.Remove(alumnoObj);
                    }
                    
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }


        public List<Alumno> mostrarAlumnos()
        {
            return Alumnos;
        }

        public bool validoPais(string paisSeleccionado)
        {
            bool retorno = true;
            
            foreach (Alumno alumnoObj in Alumnos)
            {
                if (alumnoObj.getPais().getCodIso() == paisSeleccionado)
                {
                    retorno = false;
                }
            }
            return retorno;
        }

        public bool validoCiudad(int ciudadSeleccionado)
        {
            bool retorno = true;

            foreach (Alumno alumnoObj in Alumnos)
            {
                if (alumnoObj.getCiudad().getCodTel() == ciudadSeleccionado)
                {
                    retorno = false;
                }
            }
            return retorno;
        }

    }
}
