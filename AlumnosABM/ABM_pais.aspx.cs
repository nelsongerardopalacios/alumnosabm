﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlumnosABM
{
    public partial class ABM_pais : System.Web.UI.Page
    {
        ListaPaises ListadoPaises = new ListaPaises();
        //ListaPaises a = ListadoPaises.mostrarPaises();

        //tengo que resolver herencia para compartir listados de paises




        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.CargarCombo();
            }
        }


        protected void vaciarForm()
        {
            TextNombre.Text = "";
            TextCodTel.Text = "";
            TextCodIso.Text = "";
        }

        protected void vaciarLabelsConsulta()
        {
            LabelNombre.Text = "";
            LabelCodTel.Text = "";
            LabelCodIso.Text = "";
        }

        protected void OcultarLabels()
        {
            LabelNombre.Visible = false;
            LabelNombreTexto.Visible = false;

            LabelCodTel.Visible = false;
            LabelCodTelTexto.Visible = false;

            LabelCodIso.Visible = false;
            LabelCodIsoTexto.Visible = false;
            
            //aca muestro los botones con las operaciones
            ButtonEliminar.Visible = false;
            ButtonModificar.Visible = false;
        }

        protected void MostrarLabels()
        {

            
            LabelNombre.Visible = true;
            LabelNombreTexto.Visible = true;

            LabelCodTel.Visible = true;
            LabelCodTelTexto.Visible = true;

            LabelCodIso.Visible = true;
            LabelCodIsoTexto.Visible = true;

            //aca muestro los botones con las operaciones
            ButtonEliminar.Visible = true;
            ButtonModificar.Visible = true;
        }

        protected void ButtonVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void ButtonAlta_Click(object sender, EventArgs e)
        {
            string error = "";
            int codTel;
            String nombre = TextNombre.Text;

            bool numerico = Validaciones.IsNumeric(TextCodTel.Text);

            if (numerico)
            {
                codTel = int.Parse(TextCodTel.Text);
            }
            else
            {
                codTel = -1;
            }
                
            String codIso = TextCodIso.Text;
            
            //no manejo los mensajes de error, no inserto DNIs negativos 
            //pero no lo informo.

            
            bool paisExistente = ListadoPaises.paisExiste(codIso);
            if (paisExistente)
            {
                LabelRespuesta.Text = "-Codigo de pais existente";
            }
            else
            {
                if (nombre == "")
                {
                    error += "<br/>- El nombre no puede estar vacio.";
                }
                if (codTel <= 0)
                {
                    error += "<br/>- Codigo telefonico invalido, recuerde que tiene que ser un entero positivo.";
                }
                if (codIso == "")
                {
                    error += "<br/>- El Codigo ISO no puede estar vacio.";
                }

                if (error=="")
                {
                    ListadoPaises.Add(nombre, codIso, codTel);
                    LabelRespuesta.Text = "Se ha guardado con exito";
                    vaciarForm();
                    this.CargarCombo();
                }
                else
                {
                    LabelRespuesta.Text = error;
                }

            }
        }

        protected void ButtonGuardarModificar_Click(object sender, EventArgs e)
        {
            int codTel = int.Parse(TextCodTel.Text);
            string nombre = TextNombre.Text;
            string codIso = TextCodIso.Text;

            ListadoPaises.modificarPais(nombre,codIso,codTel);

            this.vaciarForm();

            //labels de nombre
            LabelNombre.Text = nombre;


            //labels de apellido
            LabelCodIso.Text = codIso;


            //labels de dni
            //LabelDni.Text = dniSeleccionado.ToString();


            //labels de curso
            LabelCodTel.Text = codTel.ToString();

            TextCodIso.Enabled = true;
            ButtonAlta.Visible = true;
            ButtonGuardarModificar.Visible = false;
            ButtonCancelarModificar.Visible = false;
            LabelRespuesta.Text = "Se ha modificado con exito";
            this.CargarCombo();
        }

        protected void ButtonCancelarModificar_Click(object sender, EventArgs e)
        {
            this.vaciarForm();
            TextCodIso.Enabled = true;
            ButtonAlta.Visible = true;
            ButtonGuardarModificar.Visible = false;
            ButtonCancelarModificar.Visible = false;
            LabelRespuesta.Text = "Operacion Cancelada";
        }

        protected void ButtonEliminar_Click(object sender, EventArgs e)
        {
            string error = "";

            string infoCombo = DropPaises.Text;

            int indice = infoCombo.IndexOf("-");

            string paisSeleccionado = infoCombo.Substring(indice+1);

            paisSeleccionado = paisSeleccionado.Replace(" ", "");

            //validaciones

            ListaCiudades ciudades = new ListaCiudades();

            List<Ciudad> validoCiudad = ciudades.mostrarCiudadesPais(paisSeleccionado);

            ListaAlumnos alumnos = new ListaAlumnos();

            bool validoAlumno = alumnos.validoPais(paisSeleccionado);

            if (!validoAlumno)
            {

                error += "<br/>- El pais esta asociado a uno o mas alumnos, no puede ser eliminado.";
            }

            if (validoCiudad.Count > 0)
            {
                error += "<br/>- El pais esta asociado a una o unas ciudades, no puede ser eliminado.";
            }

            if (error == "")
            {

                bool resultado = ListadoPaises.EliminarPais(paisSeleccionado);

                if (resultado == true)
                {
                    LabelRespuesta.Text = "Se eliminado con exito";

                    this.CargarCombo();

                    if (ListadoPaises.obtenerListaString().Count == 0)
                    {
                        this.OcultarLabels();
                    }
                }
                else
                {
                    LabelRespuesta.Text = "Se ha producido un error.";
                }
            }
            else
            {
                LabelRespuesta.Text = error;
            }
        }

        protected void ButtonModificar_Click(object sender, EventArgs e)
        {
            string paisSeleccionado = DropPaises.Text;

            Pais paisEditar = ListadoPaises.BuscarPais(paisSeleccionado);

            TextCodIso.Enabled = false;
            TextNombre.Text = paisEditar.getNombre();
            TextCodIso.Text = paisEditar.getCodIso();
            TextCodTel.Text = paisEditar.getCodTel().ToString();

            ButtonAlta.Visible = false;
            ButtonGuardarModificar.Visible = true;
            ButtonCancelarModificar.Visible = true;

        }

        protected void DropPaises_SelectedIndexChanged(object sender, EventArgs e)
        {
            string paisSeleccionado = DropPaises.Text;

            this.BuscoPais(paisSeleccionado);
        }

        protected void CargarCombo()
        {
            DropPaises.DataSource = ListadoPaises.obtenerListaString();

            DropPaises.DataBind();

            switch (DropPaises.Items.Count)
            {
                case 0:
                    DropPaises.Visible = false;

                    LabelPaisesCargados.Visible = false;
                    break;
                case 1:
                    DropPaises.Visible = false;
                    LabelPaisesCargados.Visible = true;

                    string paisSeleccionado = DropPaises.Text;

                    this.BuscoPais(paisSeleccionado);

                    this.MostrarLabels();

                    break;
                default:
                    LabelPaisesCargados.Visible = true;
                    DropPaises.Visible = true;
                    break;
            }
        }

        protected void BuscoPais(string paisSeleccionado)
        {
            Pais pais = ListadoPaises.BuscarPais(paisSeleccionado);
            
            LabelNombre.Text = pais.getNombre();
            
            LabelCodIso.Text = pais.getCodIso();

            LabelCodTel.Text = pais.getCodTel().ToString();


            //aca muestro los botones con las operaciones
            this.MostrarLabels();
        }
    }
}