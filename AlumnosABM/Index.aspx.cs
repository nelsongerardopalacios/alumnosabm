﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AlumnosABM
{
    public partial class Index : System.Web.UI.Page
    {
        //Alumno alumno = new Alumno();
        ListaAlumnos ListadoAlumnos = new ListaAlumnos();
        ListaPaises ListadoPaises = new ListaPaises();
        ListaCiudades ListadoCiudades = new ListaCiudades(); 
        //ListaCiudades ListadoCiudades = new ListaCiudades();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.CargarCombo();
                //this.CargarComboPais();
                //this.CargarComboCiudad();
            }
                
        }

        protected void ButtonAlta_Click(object sender, EventArgs e/*,int operacion = 1*/)
        {
            string error = "";
            DateTime fechaNacimiento = DateTime.MinValue;
            int dni = 0;

            bool validoDni = Validaciones.IsNumeric(TextDni.Text);
            if(validoDni)
            {
                dni = int.Parse(TextDni.Text);
            }
            else
            {
                error += "- El DNI debe ser numerico. <br />";
            }
            
            String nombre = TextNombre.Text;
            String apellido = TextApellido.Text;
            String curso = TextCurso.Text;
            //DateTime fechaNacimiento = DateTime.Now;

            if(Validaciones.IsDate(TextBoxFechaNacimiento.Text))
            {
                fechaNacimiento = DateTime.Parse(TextBoxFechaNacimiento.Text);
            }
            else
            {
                //String fechaNacimiento;
                error += "- La fecha de nacimiento no puede estar vacia. <br />";
            }
            

            string pais = DropPaises.Text;
            Pais objPais = ListadoPaises.BuscarPais(pais);
            string ciudad = DropCiudades.Text;
            Ciudad objCiudad = ListadoCiudades.BuscarCiudad(ciudad);


            bool dniExistente = ListadoAlumnos.dniExiste(dni);
            if(dniExistente)
            {
                LabelRespuesta.Text = "-dni existente";
            }
            else
            {
                if ((dni > 0) && (nombre != "") && (apellido != "") && (curso != "") && (fechaNacimiento != DateTime.MinValue))
                {

                    ListadoAlumnos.Add(dni, nombre, apellido, curso, fechaNacimiento,objPais,objCiudad);
                    LabelRespuesta.Text = "Se ha guardado con exito";
                    vaciarForm();
                    this.CargarCombo();

                }
                else
                {
                    error += "-Alguno de los campos no fue ingresado o no es valido.";
                    LabelRespuesta.Text = error;
                }

                
            }

        }
        //YA LO PASE A PAIS
        protected void vaciarForm()
        {
            TextDni.Text = "";
            TextNombre.Text = "";
            TextApellido.Text = "";
            TextCurso.Text = "";
            //LabelFechaNacAlta.Text = "";
            TextBoxFechaNacimiento.Text = "";
        }
        //YA LO PASE A PAIS
        protected void vaciarLabelsConsulta()
        {
            LabelNombre.Text = "";
            LabelApellido.Text = "";
            LabelDni.Text = "";
            LabelCurso.Text = "";
            LabelFechaNacAlta.Text = "";
        }

        protected void ButtonGuardarModificar_Click(object sender, EventArgs e)
        {
            int dniSeleccionado = int.Parse(TextDni.Text);
            string nombre = TextNombre.Text;
            string apellido = TextApellido.Text;
            string curso = TextCurso.Text;
            //DateTime fechaNacimiento = DateTime.Now;
            DateTime fechaNacimiento = DateTime.Parse(TextBoxFechaNacimiento.Text);
            string fechaNacimientoString = fechaNacimiento.ToString();
            string pais = DropPaises.Text;
            Pais objPais = ListadoPaises.BuscarPais(pais);
            string ciudad = DropCiudades.Text;
            Ciudad objCiudad = ListadoCiudades.BuscarCiudad(ciudad);

            ListadoAlumnos.modificarAlumno(dniSeleccionado, nombre, apellido, curso,fechaNacimiento, objPais, objCiudad);

            this.vaciarForm();

            //this.OcultarLabels();

            //labels de nombre
            LabelNombre.Text = nombre;


            //labels de apellido
            LabelApellido.Text = apellido;


            //labels de dni
            //LabelDni.Text = dniSeleccionado.ToString();


            //labels de curso
            LabelCurso.Text = curso;

            //labels de fechaNacimiento
            LabelFechaNacimiento.Text = fechaNacimientoString.Substring(0,10);

            //labels de pais
            LabelPais.Text = pais;

            //labels de ciudad
            LabelCiudad.Text = ciudad;


            TextDni.Enabled = true;
            ButtonAlta.Visible = true;
            ButtonGuardarModificar.Visible = false;
            ButtonCancelarModificar.Visible = false;
            LabelRespuesta.Text = "Se ha modificado con exito";

        }
        //YA LO PASE A PAIS
        protected void BuscoDni(int dniSeleccionado)
        {
            Alumno alumno = ListadoAlumnos.BuscarAlumnoDni(dniSeleccionado);

            //labels de nombre
            LabelNombre.Text = alumno.getNombre();


            //labels de apellido
            LabelApellido.Text = alumno.getApellido();


            //labels de dni
            LabelDni.Text = alumno.getDni().ToString();


            //labels de curso
            LabelCurso.Text = alumno.getCurso();


            //labels de curso
            LabelFechaNacimiento.Text = alumno.getFechaNacimiento();
            //LabelFechaNacimiento.Text = alumno.getFechaNacimiento();
            //TextBoxFechaNacimiento.Text = alumno.getFechaNacimiento();

            //labels de pais
            //DropPaises.Text = alumno.getPais().getCodIso();
            Pais paisPrueba = alumno.getPais();
            LabelPais.Text = paisPrueba.getNombre();
            //labels de ciudad
            //DropCiudades.Text = alumno.getCiudad().getNombre();
            LabelCiudad.Text = alumno.getCiudad().getNombre();

            //aca muestro los botones con las operaciones
            this.MostrarLabels();
        }
        //YA LO PASE A PAIS
        protected void DropAlumnos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string dniSeleccionadoString = DropAlumnos.Text;
            int dniSeleccionado = int.Parse(DropAlumnos.Text);

            this.BuscoDni(dniSeleccionado);

        }
        //YA LO PASE A PAIS
        protected void ButtonEliminar_Click(object sender, EventArgs e)
        {
            int dniSeleccionado = int.Parse(DropAlumnos.Text);

            ListadoAlumnos.EliminarAlumno(dniSeleccionado);

            LabelRespuesta.Text = "Se eliminado con exito";

            this.CargarCombo();

            //labels de apellido
            if(ListadoAlumnos.obtenerListaDni().Count == 0)
            {
                this.OcultarLabels();
            }
            //this.OcultarLabels();
            //this.vaciarLabelsConsulta();

            //ACA TENGO QUE TENER EN CUENTA QUE SI EL NUMERO DE ITEMS EN EL COMBO ES UNO, TENGO QUE MOSTRAR ABAJO YA LOS DATOS O DE ALGUNA MANERA TENGO
            //QUE RESOLVER ESTE PROBLEMA


        }
        //YA LO PASE A PAIS
        protected void ButtonModificar_Click(object sender, EventArgs e)
        {
            int dniSeleccionado = int.Parse(DropAlumnos.Text);

            Alumno alumnoEditar = ListadoAlumnos.BuscarAlumnoDni(dniSeleccionado);
            
            TextDni.Text = dniSeleccionado.ToString();
            TextDni.Enabled = false;
            TextNombre.Text = alumnoEditar.getNombre();
            TextApellido.Text = alumnoEditar.getApellido();
            TextCurso.Text = alumnoEditar.getCurso();
            //LabelFechaNacAlta.Text = alumnoEditar.getFechaNacimiento();

            DateTime fecha = DateTime.Parse(alumnoEditar.getFechaNacimiento());
            //string fecha = alumnoEditar.getFechaNacimiento().ToString("yyyy-MM-dd");
            TextBoxFechaNacimiento.Text = fecha.ToString("yyyy-MM-dd"); //"22-02-1988";

            //TextBoxFechaNacimiento.

            DropPaises.SelectedValue = alumnoEditar.getPais().getCodIso();

            string codIsoSelected = alumnoEditar.getCiudad().getCodTel().ToString();

            string paisSeleccionado = DropPaises.DataValueField;

            ListadoCiudades.mostrarCiudadesPais(alumnoEditar.getPais().getCodIso());

            DropCiudades.SelectedValue = codIsoSelected;
            


            ButtonAlta.Visible = false;
            ButtonGuardarModificar.Visible = true;
            ButtonCancelarModificar.Visible = true;

        }
        //YA LO PASE A PAIS
        protected void OcultarLabels()
        {
            LabelNombre.Visible = false;
            LabelNombreTexto.Visible = false;

            //labels de apellido

            LabelApellido.Visible = false;
            LabelApellidoTexto.Visible = false;

            //labels de dni
            LabelDni.Visible = false;
            LabelDniTexto.Visible = false;

            //labels de curso

            LabelCurso.Visible = false;
            LabelCursoTexto.Visible = false;

            LabelFechaNacimientoTexto.Visible = false;
            LabelFechaNacimiento.Visible = false;

            LabelPaisTexto.Visible = false;
            LabelPais.Visible = false;

            LabelCiudadTexto.Visible = false;
            LabelCiudad.Visible = false;

            //aca muestro los botones con las operaciones
            ButtonEliminar.Visible = false;
            ButtonModificar.Visible = false;
        }
        //YA LO PASE A PAIS
        protected void MostrarLabels()
        {

            //labels nombre
            LabelNombre.Visible = true;
            LabelNombreTexto.Visible = true;

            //labels de apellido
            
            LabelApellido.Visible = true;
            LabelApellidoTexto.Visible = true;

            //labels de dni
            
            LabelDni.Visible = true;
            LabelDniTexto.Visible = true;

            //labels de curso
            
            LabelCurso.Visible = true;
            LabelCursoTexto.Visible = true;

            LabelFechaNacimientoTexto.Visible = true;
            LabelFechaNacimiento.Visible = true;

            LabelPaisTexto.Visible = true;
            LabelPais.Visible = true;

            LabelCiudadTexto.Visible = true;
            LabelCiudad.Visible = true;

            //aca muestro los botones con las operaciones
            ButtonEliminar.Visible = true;
            ButtonModificar.Visible = true;
        }

        //YA LO PASE A PAIS
        protected void ButtonCancelarModificar_Click(object sender, EventArgs e)
        {
            this.vaciarForm();
            TextDni.Enabled = true;
            ButtonAlta.Visible = true;
            ButtonGuardarModificar.Visible = false;
            ButtonCancelarModificar.Visible = false;
            LabelRespuesta.Text = "Operacion Cancelada";
        }
        //YA LO PASE A PAIS
        protected void CargarCombo()
        {
            /*ListadoAlumnos.Add(33514466,"Nelson","Palacios","programacion");
            ListadoAlumnos.Add(22980114, "Claudio", "Dominguez", "matematicas");
            ListadoAlumnos.Add(35979332, "Gisela", "Gamarra", "base de datos");*/

            //ListadoAlumnos.Add(22980114, "Claudio", "Dominguez", "matematicas");

            DropAlumnos.DataSource = ListadoAlumnos.obtenerListaDni();

            DropAlumnos.DataBind();

            switch (DropAlumnos.Items.Count)
            {
                case 0:
                    DropAlumnos.Visible = false;
                    LabelDniCargados.Visible = false;
                    break;
                case 1:
                    DropAlumnos.Visible = false;
                    LabelDniCargados.Visible = true;

                    int dniSeleccionado = int.Parse(DropAlumnos.Text);

                    this.BuscoDni(dniSeleccionado);

                    this.MostrarLabels();

                    break;
                default:
                    LabelDniCargados.Visible = true;
                    DropAlumnos.Visible = true;
                    break;
            }
        }

        /*protected void CargarComboPais()
        {
            ListadoPaises.Add("Estados Unidos","USA",01);
            ListadoPaises.Add("Uruguay", "URY", 598);
            ListadoPaises.Add("Argentina", "ARG", 54);


            /*DropCiudades.DataSource = ListadoPaises.mostrarPaises();
            //DropCiudades.DataSourceID = "Pais";
            DropCiudades.DataTextField = "nombre";
            DropCiudades.DataValueField = "codTel";


            DropCiudades.DataBind();
        }*/

        /*protected void CargarComboCiudad()
        {
            ListadoCiudades.Add("Washington D.C.", ListadoPaises.BuscarPais("USA"), 202);
            ListadoCiudades.Add("Boston", ListadoPaises.BuscarPais("USA"), 617);
            ListadoCiudades.Add("Chicago", ListadoPaises.BuscarPais("USA"), 312);
            ListadoCiudades.Add("Dallas", ListadoPaises.BuscarPais("USA"), 214);
            ListadoCiudades.Add("Montevideo", ListadoPaises.BuscarPais("URY"), 2);
            ListadoCiudades.Add("Punta del Este", ListadoPaises.BuscarPais("URY"), 42);
            ListadoCiudades.Add("Buenos Aires", ListadoPaises.BuscarPais("ARG"), 11);
            ListadoCiudades.Add("Bariloche", ListadoPaises.BuscarPais("ARG"), 2944);
            ListadoCiudades.Add("Córdoba", ListadoPaises.BuscarPais("ARG"), 351);
            ListadoCiudades.Add("Mar del Plata", ListadoPaises.BuscarPais("ARG"), 233);
            ListadoCiudades.Add("Mendoza", ListadoPaises.BuscarPais("ARG"), 261);
            ListadoCiudades.Add("San Martín de los Andes", ListadoPaises.BuscarPais("ARG"), 2972);


            //DropCiudades.DataSource = ListadoCiudades.mostrarCiudades();
            /*DropCiudades.DataSourceID = "Ciudad";
            DropCiudades.DataTextField = "nombre";
            DropCiudades.DataValueField = "codTel";


            DropCiudades.DataBind();
        }*/

        /*protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            LabelFechaNacAlta.Text = Calendar1.SelectedDate.ToShortDateString();
        }*/

        protected void Pais_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            /*string paisSeleccionado = DropPaises.DataValueField;

            //DropCiudades.DataSource = null;
            //DropCiudades.DataSourceID = ListadoCiudades.mostrarCiudadesPais(paisSeleccionado);

            DropCiudades.DataBind();
            */

        }

        protected void DropCiudades_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ButtonAbmPaises_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABM_pais.aspx");
        }

        protected void ButtonAbmCiudades_Click(object sender, EventArgs e)
        {
            Response.Redirect("ABM_ciudades.aspx");
        }
    }
}