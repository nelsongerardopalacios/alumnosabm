﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumnosABM
{
    public class ListaCiudades
    {
        static public List<Ciudad> Ciudades = new List<Ciudad>();

        public bool Add(string nombre, Pais pais, int codTel)
        {
            try
            {
                Ciudad ciudadNuevo = new Ciudad(nombre, pais, codTel);
                Ciudades.Add(ciudadNuevo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarCiudad(string nombre, Pais pais, int codTel)
        {

            try
            {
                foreach (Ciudad ciudadObj in Ciudades)
                {
                    if (ciudadObj.getCodTel() == codTel)
                    {
                        ciudadObj.Modificar(nombre, pais, codTel);
                    }

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EliminarCiudad(int codTel)
        {
            try
            {
                foreach (Ciudad ciudadObj in Ciudades)
                {
                    if (ciudadObj.getCodTel() == codTel)
                    {
                        Ciudades.Remove(ciudadObj);
                        break;
                    }

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<Ciudad> mostrarCiudades()
        {
            return Ciudades;
        }

        public List<Ciudad> mostrarCiudadesPais(string codIso)
        {
            ListaPaises ListadoPaises = new ListaPaises();

            Pais pais = ListadoPaises.BuscarPais(codIso);

            List<Ciudad> CiudadesFiltradas = new List<Ciudad>();

            foreach (Ciudad ciudadObj in Ciudades)
            {
                if (ciudadObj.getPais() == pais)
                {
                    CiudadesFiltradas.Add(ciudadObj);
                }

            }

            return CiudadesFiltradas;
        }


        public Ciudad BuscarCiudad(string ciudadSeleccionado)
        {
            Ciudad ciudad = null;
            
            int indice;
            
            indice = ciudadSeleccionado.IndexOf("-");
            

            string selecString = ciudadSeleccionado.Substring(indice + 1);

            int telInt = int.Parse(selecString);
            //int ciudadInt = int.Parse(ciudadSeleccionado);
            //string ciudadSeleccionado = DropCiudades.DataValueField;
            

            foreach (Ciudad ciudadObj in Ciudades)
            {
                //if (ciudadObj.getNombre() == ciudadSeleccionado)
                if (ciudadObj.getCodTel() == telInt)
                {
                    //cuando encuentro el alumno debo para el foreach 
                    ciudad = ciudadObj;
                    break;
                }
            }
            return ciudad;
        }

        public List<string> obtenerListaString()
        {
            List<string> listadoString = new List<string>();

            for (int i = 0; i < Ciudades.Count; i++)
            {
                int codTel = Ciudades[i].getCodTel();
                string codTelString = codTel.ToString();
                string nombre = Ciudades[i].getNombre();
                listadoString.Add(nombre + "-" + codTelString);
            }
            return listadoString;

        }

        public bool ciudadExiste(string nombre,int codTel,Pais pais)
        {
            bool retorno = false;
            foreach (Ciudad ciudadObj in Ciudades)
            {
                if ((ciudadObj.getCodTel() == codTel) && (ciudadObj.getNombre() == nombre) && (ciudadObj.getPais() == pais))
                {
                    retorno = true;
                }
            }
            return retorno;


        }
    }
}