﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumnosABM
{
    public class Ciudad
    {
        public string nombre { get; set; }
        public int codTel { get; set; }
        public Pais pais { get; set; }


        public Ciudad(string nombre,Pais pais,int codTel)
        {
            this.nombre = nombre;
            this.pais = pais;
            this.codTel = codTel;
        }

        public bool Modificar(string nombre, Pais pais, int codTel)
        {
            try
            {
                this.nombre = nombre;
                this.pais = pais;
                this.codTel = codTel;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string getNombre()
        {
            return nombre;
        }

        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public Pais getPais()
        {
            return pais;
        }

        public void setPais(Pais pais)
        {
            this.pais = pais;
        }

        public int getCodTel()
        {
            return codTel;
        }

        public void setCodTel(int codTel)
        {
            this.codTel = codTel;
        }

    }
}