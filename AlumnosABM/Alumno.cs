﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumnosABM
{
    class Alumno
    {
        private int dni { get; set; }
        private String nombre { get; set; }
        private String apellido { get; set; }
        private String curso { get; set; }
        private DateTime fechaNacimiento { get; set; }
        private Pais pais{ get; set; }
        private Ciudad ciudad { get; set; }



        public Alumno(int dni, String nombre, String apellido, String curso, DateTime fechaNacimiento, Pais pais, Ciudad ciudad)
        {
            try
            {
                this.dni = dni;
                this.nombre = nombre;
                this.apellido = apellido;
                this.curso = curso;
                this.fechaNacimiento = fechaNacimiento;
                this.pais = pais;
                this.ciudad = ciudad;

                //return true;
            }
            catch (Exception)
            {
                //return false;
            }
        }

        public bool Modificar(int dni, String nombre, String apellido, String curso, DateTime fechaNacimiento, Pais pais, Ciudad ciudad)
        {
            try
            {
                this.dni = dni;
                this.nombre = nombre;
                this.apellido = apellido;
                this.curso = curso;
                this.fechaNacimiento = fechaNacimiento;
                this.pais = pais;
                this.ciudad = ciudad;
                this.pais = pais;
                this.ciudad = ciudad;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Mostrar()
        {
            //esto no esta bien, aca tengo que devolver 
            //los datos pero los muestro por pantalla en el main
            //Console.WriteLine(this.dni);
            //return this.dni;

        }

        public int getDni()
        {
            return dni;
        }

        public void setDni(int dni)
        {
            this.dni = dni;
        }

        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public String getApellido()
        {
            return apellido;
        }

        public void setApellido(String apellido)
        {
            this.apellido = apellido;
        }

        public string getCurso()
        {
            return curso;
        }

        public void setCurso(string curso)
        {
            this.curso = curso;
        }

        /*public DateTime getFechaNacimiento()
        {
            return fechaNacimiento;
        }*/

        public string getFechaNacimiento()
        {
            return fechaNacimiento.ToShortDateString();
        }

        public void setFechaNacimiento(DateTime fechaNacimiento)
        {
            this.fechaNacimiento = fechaNacimiento;
        }

        public Pais getPais()
        {
            return this.pais;
        }

        public void setPais(Pais pais)
        {
            this.pais = pais;
        }

        public Ciudad getCiudad()
        {
            return this.ciudad;
        }

        public void setCiudad(Ciudad ciudad)
        {
            this.ciudad = ciudad;
        }

    }
}



