﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ABM_ciudades.aspx.cs" Inherits="AlumnosABM.ABM_ciudades" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="height: 318px">
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="LabelTitulo" runat="server" Text="ABM de Ciudades."></asp:Label>
        <br />
        <br />
        <asp:Label ID="LabelNombreAlta" runat="server" Text="Nombre" Width="120px"></asp:Label>
        <asp:TextBox ID="TextNombre" runat="server" Width="180px" Style="text-transform: uppercase"></asp:TextBox>
            <br />
        <asp:Label ID="LabelCodTelAlta" runat="server" Text="Codigo Telefonico" Width="120px"></asp:Label>
        <asp:TextBox ID="TextCodTel" runat="server" Width="180px"></asp:TextBox>
            <br />
        <asp:Label ID="LabelCodPaisAlta" runat="server" Text="Codigo Pais" Width="120px"></asp:Label>
        <!--<asp:TextBox ID="TextCodPais" runat="server" Width="180px" ></asp:TextBox>
        -->
        <asp:DropDownList ID="DropPaises" runat="server" DataSourceID="Pais" DataTextField="nombre" DataValueField="codIso" AutoPostBack="True" Width="184px">
            </asp:DropDownList>
        <asp:ObjectDataSource ID="Pais" runat="server" OnSelecting="Pais_Selecting" SelectMethod="mostrarPaises" TypeName="AlumnosABM.ListaPaises"></asp:ObjectDataSource>
            <br />
        <br />
        <asp:Button ID="ButtonAlta" runat="server" OnClick="ButtonAlta_Click" Text="Alta" />
        <asp:Button ID="ButtonGuardarModificar" runat="server" Text="Guardar" Visible="False" OnClick="ButtonGuardarModificar_Click" />
        <asp:Button ID="ButtonCancelarModificar" runat="server" Text="Cancelar" Visible="False" OnClick="ButtonCancelarModificar_Click"/>
        <br />
    
    </div>
    <div style="height: 464px">
    
        <asp:Label ID="LabelRespuesta" runat="server"></asp:Label>
    
        <br />
        <br />
        <asp:Label ID="LabelCiudadesCargadas" runat="server" Text="Ciudades cargadas" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="DropCiudades" runat="server" OnSelectedIndexChanged="DropCiudades_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
        
        <br />
        <div>
            <asp:Label ID="LabelNombreTexto" runat="server" Text="Nombre" Width="120px" Visible="False" ></asp:Label>
        
            <asp:Label ID="LabelNombre" runat="server" Visible="true"></asp:Label>
            <br />
            <asp:Label ID="LabelCodTelTexto" runat="server" Text="Codigo Telefonico" Width="120px" Visible="False"></asp:Label>

            <asp:Label ID="LabelCodTel" runat="server" Visible="False" ></asp:Label>
            <br />
            <asp:Label ID="LabelCodPaisTexto" runat="server" Text="Codigo Pais" Width="120px" Visible="False"></asp:Label>
            
            <asp:Label ID="LabelCodPais" runat="server"  Visible="False"></asp:Label>
            <br />
            <br />
            <br />
            <br />
            <asp:Button ID="ButtonEliminar" runat="server" OnClick="ButtonEliminar_Click" Text="Eliminar" Visible="False" />
            <asp:Button ID="ButtonModificar" runat="server" Text="Modificar" Visible="False" OnClick="ButtonModificar_Click" />
            <br />
            <br />
            <br />
            <asp:Button ID="ButtonVolver" runat="server" OnClick="ButtonVolver_Click" Text="Volver" />
        </div>
    </div>
    </form>
</body>
</html>
