﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumnosABM
{
    public class ListaPaises
    {
        static public List<Pais> Paises = new List<Pais>();

        public bool Add(string nombre, string codIso, int codTel)
        {
            try
            {
                Pais paisNuevo = new Pais(nombre, codIso,codTel);
                Paises.Add(paisNuevo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool modificarPais(string nombre, string codIso, int codTel)
        {

            try
            {
                foreach (Pais paisObj in Paises)
                {
                    if (paisObj.getCodIso() == codIso)
                    {
                        paisObj.Modificar(nombre,codIso, codTel);
                    }

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Pais BuscarPais(string paisSeleccionado)
        {
            Pais pais = null;
            
            if (paisSeleccionado != null)
            {
                bool condicionBusqueda = paisSeleccionado.Contains("-");
                
                if (condicionBusqueda)
                {
                    foreach (Pais paisObj in Paises)
                    {
                        if ((paisObj.getNombre() + "-" + paisObj.getCodIso()) == paisSeleccionado)
                        {
                            //cuando encuentro el pais debo para el foreach 
                            pais = paisObj;
                            break;
                        }
                    }

                }
                else
                {

                    string infoCombo = paisSeleccionado;

                    int indice = infoCombo.IndexOf("-");

                    string paisString = infoCombo.Substring(indice + 1);


                    foreach (Pais paisObj in Paises)
                    {
                        if (paisObj.getCodIso() == paisString)
                        {
                            //cuando encuentro el pais debo para el foreach 
                            pais = paisObj;
                            break;
                        }
                    }

                }
            }

            return pais;
        }

        public bool EliminarPais(string codIso)
        {

            //ahora tengo que tener en cuenta que puede haber alguna ciudad asociada

            try
            {
                foreach (Pais paisObj in Paises)
                {
                    if (paisObj.getCodIso() == codIso)
                    {
                        Paises.Remove(paisObj);
                        break;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }


        public List<Pais> mostrarPaises()
        {
            return Paises;
        }

        public bool paisExiste(string codIso)
        {
            bool retorno = false;
            foreach (Pais paisObj in Paises)
            {
                if (paisObj.getCodIso() == codIso)
                {
                    retorno = true;
                }
            }
            return retorno;


        }

        public List<string> obtenerListaString()
        {
            List<string> listadoString = new List<string>();

            for (int i = 0; i < Paises.Count; i++)
            {
                string codIso = Paises[i].getCodIso();
                string nombre = Paises[i].getNombre();
                listadoString.Add(nombre+"-"+codIso);
            }
            return listadoString;

        }
    }
}