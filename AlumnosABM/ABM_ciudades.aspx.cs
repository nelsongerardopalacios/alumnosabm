﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlumnosABM
{
    public partial class ABM_ciudades : System.Web.UI.Page
    {
        ListaPaises ListadoPaises = new ListaPaises();
        ListaCiudades ListadoCiudades = new ListaCiudades();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.CargarCombo();
            }
        }

        protected void vaciarForm()
        {
            TextNombre.Text = "";
            TextCodTel.Text = "";
            TextCodPais.Text = "";
        }

        protected void vaciarLabelsConsulta()
        {
            LabelNombre.Text = "";
            LabelCodTel.Text = "";
            LabelCodPais.Text = "";
        }

        protected void OcultarLabels()
        {
            LabelNombre.Visible = false;
            LabelNombreTexto.Visible = false;

            LabelCodTel.Visible = false;
            LabelCodTelTexto.Visible = false;

            LabelCodPais.Visible = false;
            LabelCodPaisTexto.Visible = false;

            //aca muestro los botones con las operaciones
            ButtonEliminar.Visible = false;
            ButtonModificar.Visible = false;
        }

        protected void MostrarLabels()
        {


            LabelNombre.Visible = true;
            LabelNombreTexto.Visible = true;

            LabelCodTel.Visible = true;
            LabelCodTelTexto.Visible = true;

            LabelCodPais.Visible = true;
            LabelCodPaisTexto.Visible = true;

            //aca muestro los botones con las operaciones
            ButtonEliminar.Visible = true;
            ButtonModificar.Visible = true;
        }

        protected void ButtonAlta_Click(object sender, EventArgs e)
        {
            string error = "";
            int codTel;
            String nombre = TextNombre.Text.ToUpper();

            bool numerico = Validaciones.IsNumeric(TextCodTel.Text);

            if (numerico)
            {
                codTel = int.Parse(TextCodTel.Text);
            }
            else
            {
                codTel = -1;
            }


            string paisString = DropPaises.Text;

            Pais pais = ListadoPaises.BuscarPais(paisString);

            //Pais pais = ListaPaises.Buscar DropPaises.Text;

            //no manejo los mensajes de error, no inserto DNIs negativos 
            //pero no lo informo.


            bool ciudadExistente = ListadoCiudades.ciudadExiste(nombre,codTel,pais);
            if (ciudadExistente)
            {
                LabelRespuesta.Text = "-La combinacion de pais, codigo telefonico y nombre es existente, modifique alguno de los parametros.";
            }
            else
            {
                if (nombre == "")
                {
                    error += "<br/>- El nombre no puede estar vacio.";
                }
                if (codTel <= 0)
                {
                    error += "<br/>- Codigo telefonico invalido, recuerde que tiene que ser un entero positivo.";
                }
                if (pais == null)
                {
                    error += "<br/>- El Codigo Pais no puede estar vacio.";
                }

                if (error == "")
                {
                    ListadoCiudades.Add(nombre, pais, codTel);
                    LabelRespuesta.Text = "Se ha guardado con exito";
                    vaciarForm();
                    this.CargarCombo();
                }
                else
                {
                    LabelRespuesta.Text = error;
                }

            }
        }

        protected void ButtonGuardarModificar_Click(object sender, EventArgs e)
        {
            string nombre = TextNombre.Text;
            int codTel = int.Parse(TextCodTel.Text);
            string paisString = DropPaises.Text;

            Pais pais = ListadoPaises.BuscarPais(paisString);

            
            ListadoCiudades.modificarCiudad(nombre,pais,codTel);
            
            this.vaciarForm();

            //this.OcultarLabels();

            //labels de nombre
            LabelNombre.Text = nombre;


            //labels de apellido
            LabelCodTel.Text = codTel.ToString();
            

            //labels de curso
            LabelCodPais.Text = paisString;

            //TextDni.Enabled = true;
            ButtonAlta.Visible = true;
            ButtonGuardarModificar.Visible = false;
            ButtonCancelarModificar.Visible = false;
            LabelRespuesta.Text = "Se ha modificado con exito";
        }

        protected void ButtonCancelarModificar_Click(object sender, EventArgs e)
        {
            this.vaciarForm();
            TextCodPais.Enabled = true;
            ButtonAlta.Visible = true;
            ButtonGuardarModificar.Visible = false;
            ButtonCancelarModificar.Visible = false;
            LabelRespuesta.Text = "Operacion Cancelada";
        }

        protected void ButtonEliminar_Click(object sender, EventArgs e)
        {
            //string error = "";

            string infoCombo = DropCiudades.Text;

            int indice = infoCombo.IndexOf("-");

            string ciudadSeleccionado = infoCombo.Substring(indice + 1);

            int ciudadInt = int.Parse(ciudadSeleccionado);

            ListaAlumnos alumnos = new ListaAlumnos();

            bool validoAlumno = alumnos.validoCiudad(ciudadInt);

            if(validoAlumno)
            {
                bool resultado = ListadoCiudades.EliminarCiudad(ciudadInt);

                if (resultado == true)
                {
                    LabelRespuesta.Text = "Se eliminado con exito";

                    this.CargarCombo();

                    if (ListadoCiudades.obtenerListaString().Count == 0)
                    {
                        this.OcultarLabels();
                    }
                }
                else
                {
                    LabelRespuesta.Text = "Se ha producido un error.";
                }
            }
            else
            {
                LabelRespuesta.Text += "<br/>- La ciudad esta asociado a uno o mas alumnos, no puede ser eliminada.";
            }
            
        }

        protected void ButtonModificar_Click(object sender, EventArgs e)
        {
            string ciudadSeleccionado = DropCiudades.Text;

            Ciudad ciudadEditar = ListadoCiudades.BuscarCiudad(ciudadSeleccionado);

            //TextCodPais.Enabled = false;
            TextNombre.Text = ciudadEditar.getNombre();
            //TextCodPais.Text = ciudadEditar.getCodPais();
            Pais pais = ciudadEditar.getPais();

            DropPaises.SelectedValue = pais.getCodIso();


            TextCodTel.Text = ciudadEditar.getCodTel().ToString();

            ButtonAlta.Visible = false;
            ButtonGuardarModificar.Visible = true;
            ButtonCancelarModificar.Visible = true;
        }

        protected void Pais_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

        }

        protected void ButtonVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void DropCiudades_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ciudadSeleccionado = DropCiudades.Text;

            this.BuscoCiudad(ciudadSeleccionado);
        }

        protected void CargarCombo()
        {
            /*DropPaises.DataSource = ListadoPaises.mostrarPaises();

            DropPaises.DataBind();*/

            DropCiudades.DataSource = ListadoCiudades.obtenerListaString();

            DropCiudades.DataBind();

            switch (DropCiudades.Items.Count)
            {
                case 0:
                    DropCiudades.Visible = false;

                    LabelCiudadesCargadas.Visible = false;
                    break;
                case 1:
                    DropCiudades.Visible = false;
                    LabelCiudadesCargadas.Visible = true;

                    string ciudadSeleccionado = DropCiudades.Text;

                    this.BuscoCiudad(ciudadSeleccionado);

                    this.MostrarLabels();

                    break;
                default:
                    LabelCiudadesCargadas.Visible = true;
                    DropCiudades.Visible = true;
                    break;
            }
        }

        protected void BuscoCiudad(string ciudadSeleccionado)
        {
            Ciudad ciudad = ListadoCiudades.BuscarCiudad(ciudadSeleccionado);

            LabelNombre.Text = ciudad.getNombre();

            LabelCodPais.Text = ciudad.getPais().getNombre();

            LabelCodTel.Text = ciudad.getCodTel().ToString();


            //aca muestro los botones con las operaciones
            this.MostrarLabels();
        }

    }
}