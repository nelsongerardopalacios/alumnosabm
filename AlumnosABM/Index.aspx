﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="AlumnosABM.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 464px">
    
        <asp:Label ID="LabelTitulo" runat="server" Text="Bienvenido al ABM de Alumnos!"></asp:Label>
        <br />
        <br />
        <div>
        <asp:Label ID="LabelNombreAlta" runat="server" Text="Nombre" Width="120px"></asp:Label>
        &nbsp;<asp:TextBox ID="TextNombre" runat="server" Width="180px"></asp:TextBox>
            <br />
        <asp:Label ID="LabelApellidoAlta" runat="server" Text="Apellido" Width="120px"></asp:Label>
        &nbsp;<asp:TextBox ID="TextApellido" runat="server" Width="180px"></asp:TextBox>
            <br />
        <asp:Label ID="LabelDNIAlta" runat="server" Text="DNI" Width="120px"></asp:Label>
        &nbsp;<asp:TextBox ID="TextDni" runat="server" Width="180px" ></asp:TextBox>
            <br />
        <asp:Label ID="LabelCursoAlta" runat="server" Text="Curso" Width="120px"></asp:Label>
        &nbsp;<asp:TextBox ID="TextCurso" runat="server" Width="180px"></asp:TextBox>
            <br />
            <asp:Label ID="LabelPaisAlta" runat="server" Text="Pais" Width="124px"></asp:Label>
            <asp:DropDownList ID="DropPaises" runat="server" DataSourceID="Pais" DataTextField="nombre" DataValueField="codIso" AutoPostBack="True" Width="184px">
            </asp:DropDownList>
            <asp:Button ID="ButtonAbmPaises" runat="server" OnClick="ButtonAbmPaises_Click" Text="ABM Paises" />
            <asp:ObjectDataSource ID="Pais" runat="server" OnSelecting="Pais_Selecting" SelectMethod="mostrarPaises" TypeName="AlumnosABM.ListaPaises"></asp:ObjectDataSource>
            <br />
            <asp:Label ID="LabelCiudadAlta" runat="server" Text="Ciudad" Width="124px"></asp:Label>
            <asp:DropDownList ID="DropCiudades" runat="server" DataSourceID="Ciudad" DataTextField="nombre" DataValueField="codTel" OnSelectedIndexChanged="DropCiudades_SelectedIndexChanged" AutoPostBack="True" Width="184px">
            </asp:DropDownList>
            <asp:Button ID="ButtonAbmCiudades" runat="server" OnClick="ButtonAbmCiudades_Click" Text="ABM Ciudades" />
            <asp:ObjectDataSource ID="Ciudad" runat="server" SelectMethod="mostrarCiudadesPais" TypeName="AlumnosABM.ListaCiudades">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropPaises" Name="codIso" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:Label ID="LabelFechaNacimientoAlta" runat="server" Text="Fecha Nacimiento: " Width="120px"></asp:Label>
            <asp:TextBox ID="TextBoxFechaNacimiento" runat="server" TextMode="Date" ></asp:TextBox>
            <asp:Label ID="LabelFechaNacAlta" runat="server"></asp:Label>
            <br />
        </div>
        <asp:Button ID="ButtonAlta" runat="server" OnClick="ButtonAlta_Click" Text="Alta" />
        <asp:Button ID="ButtonGuardarModificar" runat="server" Text="Guardar" Visible="False" OnClick="ButtonGuardarModificar_Click" />
        <asp:Button ID="ButtonCancelarModificar" runat="server" Text="Cancelar" Visible="False" OnClick="ButtonCancelarModificar_Click"/>
        <br />
        <asp:Label ID="LabelRespuesta" runat="server"></asp:Label>
    
        <br />
        <br />
        <asp:Label ID="LabelDniCargados" runat="server" Text="DNIs cargados" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="DropAlumnos" runat="server" OnSelectedIndexChanged="DropAlumnos_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
        
        <br />
        <div>
            <asp:Label ID="LabelNombreTexto" runat="server" Text="Nombre" Width="120px" Visible="False"></asp:Label>
        
            <asp:Label ID="LabelNombre" runat="server" Visible="true"></asp:Label>
            <br />
            <asp:Label ID="LabelApellidoTexto" runat="server" Text="Apellido" Width="120px" Visible="False"></asp:Label>

            <asp:Label ID="LabelApellido" runat="server" Visible="False" ></asp:Label>
            <br />
            <asp:Label ID="LabelDniTexto" runat="server" Text="DNI" Width="120px" Visible="False"></asp:Label>
            
            <asp:Label ID="LabelDni" runat="server"  Visible="False"></asp:Label>
            <br />
            <asp:Label ID="LabelCursoTexto" runat="server" Text="Curso" Width="120px" Visible="False"></asp:Label>
            
            <asp:Label ID="LabelCurso" runat="server"  Visible="False"></asp:Label>
            <br />
            <asp:Label ID="LabelFechaNacimientoTexto" runat="server" Text="Fecha Nacimiento" Width="120px" Visible="False"></asp:Label>
            <asp:Label ID="LabelFechaNacimiento" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="LabelPaisTexto" runat="server" Text="Pais" Width="120px" Visible="False"></asp:Label>
            <asp:Label ID="LabelPais" runat="server"></asp:Label>
            <br />
            <asp:Label ID="LabelCiudadTexto" runat="server" Text="Ciudad" Width="120px" Visible="False"></asp:Label>
            <asp:Label ID="LabelCiudad" runat="server"></asp:Label>
            <br />
            <asp:Button ID="ButtonEliminar" runat="server" OnClick="ButtonEliminar_Click" Text="Eliminar" Visible="False" />
            <asp:Button ID="ButtonModificar" runat="server" Text="Modificar" Visible="False" OnClick="ButtonModificar_Click" />
            <br />
            <br />
            <br />
        </div>
    </div>
    </form>
</body>
</html>
