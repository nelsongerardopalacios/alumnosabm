﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumnosABM
{
    class Validaciones
    {
        //metodo para validar si los valores son numericos
        public static bool IsNumeric(String num)
        {
            try
            {
                int numero = int.Parse(num);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsDate(String date)
        {
            try
            {
                DateTime fecha = Convert.ToDateTime(date);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
