﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumnosABM
{
    public class Pais
    {
        public string nombre { get; set; }
        public int codTel { get; set; }
        public string codIso { get; set; }


        public Pais(string nombre,string codIso,int codTel)
        {
            this.nombre = nombre;
            this.codIso = codIso;
            this.codTel = codTel;
        }

        public bool Modificar(string nombre, string codIso, int codTel)
        {
            try
            {
                this.nombre = nombre;
                this.codIso = codIso;
                this.codTel = codTel;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string getNombre()
        {
            return nombre;
        }

        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public string getCodIso()
        {
            return codIso;
        }

        public void setCodIso(string codIso)
        {
            this.codIso = codIso;
        }

        public int getCodTel()
        {
            return codTel;
        }

        public void setCodTel(int codTel)
        {
            this.codTel = codTel;
        }

    }
}